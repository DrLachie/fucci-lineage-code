/*   
 *    
 *    	Run this macro after cells have been tracked using trackmate. 
 *    	Once tracked you will need the saved output files from trackmate (Spots in tracks statistics file). 
 *    	
 *    	Select the tif file (acquired as per method), green channel spots file and red channel spots file when prompted. 
 *    	
 *    	Macro will create a montage image with merged channels and track numbers labelled. Use this montage to manually
 *    	create lineage linkages. 
 *    
 *    
 */

	//if roi manager is open with regions it can cause issues
	if(isOpen("ROI Manager")){
		selectWindow("ROI Manager");
		run("Close");
	}
	run("Close All");
	
	//propt for required files
	filePath = File.openDialog("choose tif file");
	c1trackFile = File.openDialog("Green channel");
	c2trackFile = File.openDialog("Red Channel");
	
	open(filePath);
	getDimensions(x,y,c,z,totalT);
	fname = getTitle();
	run("Split Channels");
		
	c1 = "C1-"+fname;
	c2 = "C2-"+fname;
	c3 = "C3-"+fname;
	run("Merge Channels...", "c1="+c1+" c2="+c2+" c4="+c3+" create keep");
	rename("merge");
	
	f0=1;
	selectWindow(c1);resetMinAndMax();run("Green");run("Set... ", "zoom=400 x=40 y=39");Stack.setFrame(f0);
	getDimensions(x,y,z,t,z);
	selectWindow(c2);resetMinAndMax();run("Red");run("Set... ", "zoom=400 x=40 y=39");Stack.setFrame(f0);
	selectWindow(c3);resetMinAndMax();run("Grays");run("Set... ", "zoom=400 x=40 y=39");Stack.setFrame(f0);
	selectWindow("merge");resetMinAndMax();
	
	Stack.setChannel(1);setMinAndMax(750,2500);run("Green");
	Stack.setChannel(2);setMinAndMax(400,2000);run("Red");
		
	run("Flatten");run("Set... ", "zoom=400 x=40 y=39");Stack.setFrame(f0);
	
	run("Tile");
	selectWindow("merge");

	//create overlays with labaled regions for each track
	newImage("both_channels", "RGB Color", x, y, totalT);
	makeOverlayMovie("Green Overlay",x,y,totalT,true,false);
	run("Canvas Size...", "width="+x+10+" height="+y+10+" position=Center zero");
	makeOverlayMovie("Red Overlay",x,y,totalT,false,true);
	run("Canvas Size...", "width="+x+10+" height="+y+10+" position=Center zero");
	selectWindow("merge");
	
	if(isOpen("ROI Manager")){
		selectWindow("ROI Manager");
		run("Close");
	}

	//combine Stacks horizontally
	run("Canvas Size...", "width="+x+10+" height="+y+10+" position=Center zero");
	run("Combine...", "stack1=[Green Overlay] stack2=[Red Overlay]");
	run("Combine...", "stack1=[Combined Stacks] stack2=[merge]");




function drawRegion(trackFile,t,color){
	run("Colors...", "foreground=white background=black selection="+color);
	getPixelSize(unit,pixel,pixel);
	tf = File.openAsString(trackFile);
	lines = split(tf,'\n');
	for(i=0;i<lines.length;i++){
		line = split(lines[i],'\t');
		x = parseInt(line[5]);y = parseInt(line[6]); spot_t = parseInt(line[9]); d = line[19]; idx = line[3];
		x = x/pixel;
		y = y/pixel;
		call("ij.plugin.filter.ParticleAnalyzer.setFontSize", 12);
		if(spot_t == t-1){			
			makeOval(x-7, y-7, 14, 14); //spot detector used 14 pixel region. 
			roiManager("Add");
			roiManager("Select",roiManager("count")-1);
			roiManager("Rename", idx);
			roiManager("Set Color", color);
			roiManager("Set Line Width", 1);
			roiManager("UseNames", "true");
		}		
	}
	roiManager("Show All with Labels");
}


function makeOverlayMovie(movieTitle,xsize,ysize,tSize,DoGreen,DoRed){
	newImage(movieTitle, "RGB Color", xsize, ysize, tSize);
	for(i=1;i<totalT;i++){
		f0=i;
		selectWindow("merge");
		Stack.setFrame(i);
	
		if(roiManager("Count")>0){
			roiManager("Deselect");
			roiManager("Delete");
		}
		if(DoGreen){drawRegion(c1trackFile,f0,"green");}
		if(DoRed){drawRegion(c2trackFile,f0,"red");}
		if(DoGreen && DoRed){roiManager("Show All without Labels");run("Select None");}
		
		if(roiManager("Count")>0){
			run("Flatten", "slice");
		}else{
			run("Duplicate...","title=asdf");
		}		
			run("Select All");
			run("Copy");
			close();
			selectWindow(movieTitle);
			Stack.setSlice(i);
			run("Paste");	
	}
	
}









